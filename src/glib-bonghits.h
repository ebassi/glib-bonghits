#ifndef __GLIB_BONGHITS_H__
#define __GLIB_BONGHITS_H__

#if !defined(GLIB_ENABLE_BONGHITS) && !defined(GLIB_BONGHITS_COMPILATION)
#error "You must enable bonghits to use glib-bonghits.h"
#endif

#define __GLIB_BONGHITS_H_INSIDE__

#undef __GLIB_BONGHITS_H_INSIDE__

#endif /* __GLIB_BONGHITS_H__ */
